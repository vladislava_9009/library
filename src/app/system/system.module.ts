import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Sharemodule } from '../shared/shared.module';
import { SystemRoutingModule} from './system-routing.module';

import { SystemComponent } from './system.component';
import { BooksComponent } from './books/books.component';
import { UsersComponent } from './users/users.component';
import { SettingsComponent } from './settings/settings.component';
import { AuthorsComponent } from './authors/authors.component';


@NgModule({
    imports: [
        CommonModule,
        Sharemodule,
        SystemRoutingModule
    ],
    declarations: [
        SystemComponent,
        BooksComponent,
        UsersComponent,
        SettingsComponent,
        AuthorsComponent,
        
    ]
})

export class SystemModule {}