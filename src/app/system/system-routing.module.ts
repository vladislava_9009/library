import { NgModule } from "@angular/core";
import { RouterModule, Routes} from '@angular/router';

import { SystemComponent} from './system.component'
import { SettingsComponent } from './settings/settings.component';
import { AuthorsComponent } from './authors/authors.component';
import { UsersComponent } from './users/users.component';
import { BooksComponent } from './books/books.component';

const routes: Routes =[
    {path:'system',component:SystemComponent, children:[
        {path:'books', component: BooksComponent},
        { path:'users', component: UsersComponent},
        { path:'authors', component: AuthorsComponent},
        { path:'settings', component: SettingsComponent},
    ]}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })

export class SystemRoutingModule {}