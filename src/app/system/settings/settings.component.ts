import { Component, OnInit } from '@angular/core';
import { IUserModel } from 'src/app/shared/models/user.model';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  
  user:IUserModel

  constructor() { }

  ngOnInit() {

    this.user=JSON.parse(window.localStorage.getItem('user')).user
    console.log(this.user)
  }

}
