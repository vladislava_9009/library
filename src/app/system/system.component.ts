import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';
import { Router } from '@angular/router';
import { UserService } from '../shared/services/users.service';
import { IUserModel } from '../shared/models/user.model';


@Component({
    selector:'app-system',
    templateUrl: './system.component.html',
    styleUrls: ['./system.component.css']
})
export class SystemComponent implements OnInit {

    constructor(
        private authServise:AuthService,
        private router:Router, 
        private userService: UserService,
    ){}
    user:IUserModel

    ngOnInit(){
      
        this.user=JSON.parse(window.localStorage.getItem('user')).user
    }

    onLogout(){
        this.authServise.logout();
        this.router.navigate(['/login'])
    }

    getProfile(){
        this.userService.getPRofile().subscribe(res =>{
            console.log(res)
        })
    }


}