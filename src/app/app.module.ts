import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AlertModule } from 'ngx-bootstrap';



import { AppRoutingModule} from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HomeModule } from './auth/home.module';
import { SystemModule } from './system/system.module'

import { HttpClientModule } from '@angular/common/http';
import { UserService } from './shared/services/users.service';
import { AuthService } from './shared/services/auth.service';
import { AdminModule } from './admin/admin.module';
import { UserModule } from './user/user.module';







@NgModule({
  declarations: [
    AppComponent,
          
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    AlertModule.forRoot(),
    HomeModule,
    HttpClientModule,
    SystemModule,
    AdminModule,
    UserModule
  ],
  providers: [UserService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
