export class AuthService {

    private isAuthenicated = false;

    login() {
        this.isAuthenicated=true;
    }

    logout() {
        this.isAuthenicated=false;
        window.localStorage.clear()
    }

    isLogedIn() {
        return this.isAuthenicated
    }
}