import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable()
export class UserService{

    constructor (private http:HttpClient) {}    

    getUserByEmail(data): Observable<any>{
         return this.http.post(`https://localHost:3000/auth/login`, data)
        
    }

    createNewUser(user): Observable<any>{
        let body = user;
        return this.http.post(`https://localHost:3000/auth/register`, body)
    }

    getProfile(): Observable<any>{
        return this.http.get(`https://localhost:3000/auth/profile`, {headers: {"x-access-token": JSON.parse(localStorage.getItem('user')).token } } )
    }

    getUserById(data): Observable<any>{
        return this.http.get(`https://localHost:3000/user`,data)
    }

    deleteUserById(_id):Observable<any>{
        return this.http.delete(`https://localHost:3000/auth/profile/`+_id)
    }

    
}