import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ngModuleJitUrl } from '@angular/compiler';

@NgModule({
    imports: [ReactiveFormsModule,FormsModule],
    exports: [ReactiveFormsModule,FormsModule]
})

export class Sharemodule {}