import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { UserService } from 'src/app/shared/services/users.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  constructor( 
    private userServise:UserService,
    private authServise:AuthService,
    private router:Router
  ) { }

  ngOnInit() {
    this.form=new FormGroup({
      'email': new FormControl(null,[Validators.required, Validators.email]),
      'password': new FormControl(null,[Validators.required,Validators.minLength(8)])
    })
  }

  onSubmit(){
    
    
    this.userServise.getUserByEmail(this.form.value).subscribe(res=>{
      this.authServise.login();
      (res.user.role==='admin')?this.router.navigate(['/admin']):this.router.navigate(['/user'])
      window.localStorage.setItem('user',JSON.stringify(res));
    },req=>(req.status===400)? alert('User is not foud'): alert('Password is not correct')
    )}

    



}
