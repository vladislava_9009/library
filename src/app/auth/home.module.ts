import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginComponent} from './login/login.component';
import { RegistrationComponent} from './registration/registration.component';
import { HomeComponent } from './home.component';
import { HomeRoutingModule} from './home-routing.module'
import { Sharemodule } from '../shared/shared.module';



@NgModule ({
 declarations:[
    LoginComponent,
    RegistrationComponent,
    HomeComponent
 ],
 imports:[
     CommonModule,
     HomeRoutingModule,
     Sharemodule
 ]
})

export class HomeModule {} 