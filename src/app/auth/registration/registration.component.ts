import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,  Validators } from '@angular/forms';
import { UserService } from 'src/app/shared/services/users.service';
import { reject } from 'q';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  form:FormGroup;

  constructor( 
    private userservice:UserService,
    private router:Router
    ) { }

  ngOnInit() {

  this.form=new FormGroup({
      'email': new FormControl(null,[Validators.required, Validators.email]),
      'fullName': new FormControl(null,[Validators.required]),
      'password': new FormControl(null,[Validators.required,Validators.minLength(8)]),
      
    })
  }

  onSubmit(){
    console.log(this.form)
    
    this.userservice.createNewUser(this.form.value).subscribe(res =>{  
    })
    this.router.navigate(['./login'])

    // verificationEmail(control:FormData) : Promise<any>{
    //   return new Promise ((resolve,reject)=>{
    //     this.usersService.getUserByEmail()
    //   })
    // }

    
  }

}
