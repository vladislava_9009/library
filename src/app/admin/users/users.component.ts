import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/services/users.service';
import { IUserModel } from 'src/app/shared/models';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  constructor(
    private userService:UserService,
  ) { }

  users:Array<IUserModel>
 

  ngOnInit() {
    this.userService.getProfile().subscribe(res =>{
      console.log(res)
    this.users=res
    
   })
  }

  deleteUser(id:string){
    this.userService.deleteUserById(id).subscribe(res=>{
    console.log(res)   
    })

    this.userService.getProfile().subscribe(res =>{
      this.users=res
     })
  }

 


}
