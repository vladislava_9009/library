import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/shared/services/users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent  {
  form:FormGroup;

  constructor( 
    private userservice:UserService,
    private router:Router
    ) { }

  ngOnInit() {

  this.form=new FormGroup({
      'email': new FormControl(null,[Validators.required, Validators.email]),
      'fullName': new FormControl(null,[Validators.required]),
      'password': new FormControl(null,[Validators.required,Validators.minLength(8)]),
      
    })
  }

  onSubmit(){
    this.userservice.createNewUser(this.form.value).subscribe(res =>{})
    alert('user added')
    this.form.reset();
         

  }
 
  
}
