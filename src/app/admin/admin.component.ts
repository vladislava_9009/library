import { Component, OnInit } from '@angular/core';
import { IUserModel } from '../shared/models/user.model';
import { UserService } from '../shared/services/users.service';
import { AuthService } from '../shared/services/auth.service';
import { Router } from '@angular/router';



@Component({
    selector:'app-admin',
    templateUrl: './admin.component.html',
    
})
export class AdminComponent implements OnInit  {

    constructor(
        private userService: UserService,
        private authServise: AuthService,
        private router:Router
    ){}

    user:IUserModel ;

ngOnInit(){
        this.user=JSON.parse(window.localStorage.getItem('user')).user
    }


onLogout(){
    this.authServise.logout();
    this.router.navigate(['/login'])
}

}