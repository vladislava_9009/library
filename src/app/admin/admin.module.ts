import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Sharemodule } from '../shared/shared.module';
import { AdminRoutingModule} from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { UsersComponent } from './users/users.component';
import { AddUserComponent } from './users/add-user/add-user.component';




@NgModule({
    imports: [
        CommonModule,
        Sharemodule,
        AdminRoutingModule
    ],
    declarations: [
        AdminComponent,
        UsersComponent,
        AddUserComponent,
        
    ]
})

export class AdminModule {}