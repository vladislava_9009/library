import { Component, OnInit } from '@angular/core';
import { IUserModel } from '../shared/models/user.model';
import { AuthService } from '../shared/services/auth.service';
import { Router } from '@angular/router';



@Component({
    selector:'app-user',
    templateUrl: './user.component.html',
    
})
export class UserComponent implements OnInit {
    constructor(
        private authServise:AuthService,
        private router:Router
    ){

    }

    user:IUserModel ;

    ngOnInit(){
      
        this.user=JSON.parse(window.localStorage.getItem('user')).user
    }   
    onLogout(){
        this.authServise.logout();
        this.router.navigate(['/login'])
    }
}