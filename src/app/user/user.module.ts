import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Sharemodule } from '../shared/shared.module';
import { UserRoutingModule} from './user-routing.module';
import { UserComponent } from './user.component';




@NgModule({
    imports: [
        CommonModule,
        Sharemodule,
        UserRoutingModule
    ],
    declarations: [
        UserComponent
        
        
    ]
})

export class UserModule {}